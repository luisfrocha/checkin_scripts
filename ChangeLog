2019-09-03  luisfernando.rocha <luisfernando.rocha@usaa.com>

    * V0.12

    ****Affects:

    Modified:
    * git-checkin0
      - Updated file name for version number
    * git-checkin2
      - Correctly get last number from version number in file

2019-09-03  luisfernando.rocha <luisfernando.rocha@usaa.com>

    * V0.11

    ****Affects:

    Modified:
    * git-checkin0
      - Updated file name for version number
      - If changes are detected, you can Add (to stage), Continue without staging them, or Exit (previously it would be Yes/No, but would continue)
      - If the ChangeLog or version.txt files don't exist, they are created
      - Fixed command where temp file was created but never removed
    * git-checkin1
      - Added flag to difftool command to prevent individual ask for opening git.difftool
    * git-checkin2
      - Updated file name for version number
      - Trim whitespace from branch URL
      - Correctly get last number from version number in file
      - After asking if ready to push to remote, stage only new ChangeLog and version files, instead of ANY changed file
    Renamed:
    * version.c -> version.txt

2017-04-12  luisfrocha <luisfrocha@icloud.com>

    * V0.10

    ****Affects:

    Modified:
    * git-checkin0
      - Reset ChangeLog and version.c if staged changes are removed
    * git-checkin1
      - Changed command of diff tool to use proper git command

2017-04-12  luisfrocha <luisfrocha@icloud.com>

    * V0.9

    ****Affects:

    Modified:
    * git-checkin2
      - Update version.c at a different point of the script
    * version.c

2017-04-12  luisfrocha <luisfrocha@icloud.com>

    * V0.8

    ****Affects:

    Modified:
    * ChangeLog
    * git-checkin0
      - No longer updating version.c
    * version.c

2017-04-12  luisfrocha <luisfrocha@icloud.com>

    * V0.7

    ****Affects:

    Modified:
    * git-checkin0
      - Changed temp file for changelog storage to prevent conflict with temp folder
    * git-checkin2
      - Added update to version file
    *version.c

2017-04-10  luisfrocha <luisfrocha@icloud.com>

    * V0.6

    ****Affects:

    Modified:
    * ChangeLog
    * git-checkin0
      - Fixed algorithm to create new version number
    * git-checkin2
      - Fixed algorithm to create new version number
    * version.c

2017-04-10  luisfrocha <luisfrocha@icloud.com>

    * V0.5

    ****Affects:

    Modified:
    * git-checkin0
    * git-checkin2
    *version.c

2017-04-06  luisfrocha <luisfrocha@icloud.com>

    * V0.4

    ****Affects:

    Modified:
    * ChangeLog
    * git-checkin0
      - Allow clearing staged changes
      - Exit if no changes detected
      - Wrap email in angle brackets
    * git-checkin1
      - Instead of asking, immediately open git diff tool
    * git-checkin2
      - Get the url for the push branch
      - Add changed files (ChangeLog, version.c, & work.diff)
      - Get current branch and push only to that one
    * version.c

2017-03-11  luisfrocha luisfrocha@icloud.com

    * V0.3

    ****Affects:
      - Split script into three different processes

    Modified:
    * ChangeLog
    *version.c

    Added:
    * git-checkin1
    * git-checkin2

    Renamed:
    * checkin -> git-checkin0

2017-03-07  luisfrocha luisfrocha@icloud.com

    * V0.2

    ****Affects:

    Modified:
    *checkin
      - Get username from git settings
      - If unstaged changes detected, output list of files
      - If first time saving ChangeLog, get entire contents for commit message
    *version.c

2017-03-07  luisfrocha luisfrocha@icloud.com

    * V0.1

    ****Affects:
      - Initial commit. Successfully updates ChangeLog, version.c, and checks in all changes

    Added:
    * .gitignore
    * ChangeLog
    * checkin
    * version.c
    Deleted:
    * checkin0
    * checkin1
    * checkin2
