#!/bin/bash

WHOAMI=$(git config user.name)
USEREMAIL=$(git config user.email)
EMAILSERVER=$(git config user.email | grep -o "@[[:alnum:][:graph:]]*" | sed -e 's/@//g')
USERNAME=$(git config user.email | sed 's/@.*//')
VERSION_FILE="version.txt"
CHANGELOG_FILE="ChangeLog"
CURRENT_DATE=$(date +%Y-%m-%d)

function exit_request {
    case $1 in
        0 )
            ;;
        1 )
            ;;
        2 ) echo "Local Configuration error"
            ;;
        3 ) echo "User request exit"
            ;;
        4 )
            ;;
        * )
            ;;
    esac

    if [ -n "$2" ]; then
        echo "$2"
    fi

    exit $1
}

if [ "$USERNAME" == "" ] || [ "$USEREMAIL" == "" ] || echo $EMAILSERVER | grep -oq "$HOSTNAME"; then
    exit_request 4 "Please use 'git config user.name' \"FirstName LastName\" and 'git config user.email \"emailusername@email.com>\" "
fi

if [[ `git diff --cached` != "" ]]; then
    echo "$(git diff --cached)"
    echo "Changes already staged. Do you want to unstage them?"
    echo -n "Ok? [y/n] "
    read x
    if [[ "$x" = "y" || "$x" = "Y" || "$x" = "yes" ]]; then
        git reset
        git checkout $CHANGELOG_FILE
        git checkout $VERSION_FILE
    fi
fi

if [[ $(git diff --cached) == "" && $(git status --short) != "" ]]; then
    echo "$(git status --short)"
    echo "Changes detected. Do you want to add them to stage?"
    echo -n "Ok? [A]dd/[C]ontinue (without adding)/[E]xit "
    read x
    x="$(echo $x | tr '[:upper:]' '[:lower:]')"
    if [[ "$x" = "a" || "$x" = "add" ]]; then
        git add .
    elif [[ "$x" = "e" || "$x" = "exit" ]]; then
        exit 0
    fi
elif [[ $(git status --short) == "" ]]; then
    exit_request 4 "No changes detected"
fi

if [ ! -f "$CHANGELOG_FILE" ]; then
    echo "$CHANGELOG_FILE does not exist. Creating..."
    echo "" >> $CHANGELOG_FILE
fi

lasttag=`git describe --tags --abbrev=0`

if [ "$lasttag" = "" ]; then
    lasttag="0.0.0"
fi
if [ ! -f "$VERSION_FILE" ]; then
    echo "$VERSION_FILE does not exist. Creating..."
    echo "version: $lasttag" >> $VERSION_FILE
fi

version_line=`grep -i version $VERSION_FILE | grep version: | awk '{print $2}'`
if [ "$version_line" = "" ]; then
    version_line=`head -1 $VERSION_FILE`
fi

ver=`expr "$version_line" : '[^0-9]*\([0-9._]*\)'`

if [[ `echo $ver | grep -Eq '^[-+]?[0-9]+\.?[0-9]*$'` = "" ]]; then
    pre=${ver%.*}
    post=`cut -d "." -f 2 <<< "$ver"`
    post=$(echo "$post + 1" | bc)
    temptag="$pre.$post"
else
    temptag=$(echo "$ver + 1" | bc)
fi

lasttag=$(expr $lasttag : '[^0-9]*\([0-9._]*\)')
echo "Current Version: V"$lasttag
echo "Updated Version: V"$temptag

# cp "$VERSION_FILE" "$VERSION_FILE.temp"
# echo "version: $temptag" > "$VERSION_FILE.temp"
# mv -f "$VERSION_FILE.temp" $VERSION_FILE
# rm -f $VERSION_FILE~*

git_status=$(git status --short | sed -e 's/^[\t]*//')
modified_files=""
added_files=""
deleted_files=""
renamed_files=""
copied_files=""
updated_unmerged_files=""
untracked_files=""
ignore=0
while read -r line; do
    status=${line:0:1}
    file=${line:2}
    if [[ "$file" = "ChangeLog" || "$file" = "work.diff" ]]; then
        continue
    fi
    case "$status" in
        "M" )
            modified_files="$modified_files
    *$file"
            ;;
        "A" )
            added_files="$added_files
    *$file"
            ;;
        "D" )
            deleted_files="$deleted_files
    *$file"
            ;;
        "R" )
            renamed_files="$renamed_files
    *$file"
            ;;
        "C" )
            copied_files="$copied_files
    *$file"
            ;;
        "U" )
            updated_unmerged_files="$updated_unmerged_files
    *$file"
            ;;
        "?" )
            untracked_files="$untracked_files
    *$file"
            ;;
    esac
done <<< "$git_status"

msg_header="$CURRENT_DATE  $USERNAME <$USEREMAIL>"
msg_version="

    * V$temptag
"
msg_affects="
    ****Affects:
"
if [ "$modified_files" != "" ]; then
    msg_changed_files="$msg_changed_files
    Modified:$modified_files"
fi
if [ "$added_files" != "" ]; then
    msg_changed_files="$msg_changed_files
    Added:$added_files"
fi
if [ "$deleted_files" != "" ]; then
    msg_changed_files="$msg_changed_files
    Deleted:$deleted_files"
fi
if [ "$renamed_files" != "" ]; then
    msg_changed_files="$msg_changed_files
    Renamed:$renamed_files"
fi
if [ "$copied_files" != "" ]; then
    msg_changed_files="$msg_changed_files
    Copied:$copied_files"
fi
if [ "$updated_unmerged_files" != "" ]; then
    msg_changed_files="$msg_changed_files
    Updated but Unmerged:$updated_unmerged_files"
fi
if [ "$untracked_files" != "" ]; then
    msg_changed_files="$msg_changed_files
    Untracked:$untracked_files"
fi
msg_footer=""
msg="$msg_header""$msg_version""$msg_affects""$msg_changed_files""$msg_footer"

cp $CHANGELOG_FILE "$CHANGELOG_FILE.temp"
# echo "$msg" >> "$CHANGELOG_FILE.temp"
echo "$msg
" | cat - "$CHANGELOG_FILE.temp" >> chkn
mv chkn "$CHANGELOG_FILE.temp"
mv -f "$CHANGELOG_FILE.temp" $CHANGELOG_FILE
rm -f ChangeLog.~*

git remote -v
echo ""
git diff --cached > work.diff
